LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_RESOURCE_DIR += \
	$(LOCAL_PATH)/res

LOCAL_PACKAGE_NAME := BlueGps
#LOCAL_CERTIFICATE := platform

#LOCAL_SDK_VERSION := current

LOCAL_PROGUARD_ENABLED := disabled

include $(BUILD_PACKAGE)

include $(call all-makefiles-under, $(LOCAL_PATH))
